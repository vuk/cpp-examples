#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>

// function to display the current time in a loop
void displayClock() {
    while (true) {
        // get the current time
        auto now = std::chrono::system_clock::now();
        auto in_time_t = std::chrono::system_clock::to_time_t(now);
        std::tm *ltm = std::localtime(&in_time_t);

        // display the date and time in the format HH:MM:SS DD-MM-YYYY
        std::cout << std::put_time(ltm, "%T %d-%m-%Y") << std::endl;

        // wait for one second before updating again
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

int main() {
    // create a thread to display the clock
    std::thread clockThread(displayClock);

    // do other work in the main thread if needed

    // wait for the clock thread to finish
    clockThread.join();

    return 0;
}